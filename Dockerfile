FROM postgres

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app

COPY ./util/up_script.sql /docker-entrypoint-initdb.d/
