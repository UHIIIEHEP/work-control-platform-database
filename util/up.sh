#!/bin/bash

source .env

PGPASSWORD=$APP_DB_PASSWORD psql -U $APP_DB_USERNAME -h $APP_DB_HOST -p $APP_DB_PORT -d $APP_DB_NAME < ./util/$FILE_NAME
