


        do $$
          begin

            create schema if not exists pkg_auth;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_competent;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_db_util;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_department;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_element;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_object;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_organisation;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_permission;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_qualification;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_session;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_task;

          end
        $$;
      


        do $$
          begin

            create schema if not exists pkg_user;

          end
        $$;
      


        do $$
          begin

            create schema if not exists public;

          end
        $$;
      

-- select pkg_db_util.pg_constraint_exists('public', 'organisation', 'uk_organisation_name')::integer;

create or replace function pkg_db_util.pg_constraint_exists(
  p_schema character varying,
  p_table character varying,
  p_constraint character varying
)
returns boolean as
$$

begin

    if not exists (
      select
        pgc.*
      from
        information_schema.table_constraints pgc
      where
        pgc.table_schema = p_schema and
        pgc.table_name = p_table and
        pgc.constraint_name = p_constraint
      )
    then

      return false;

    else

      return true;

    end if;
end;
$$
  language plpgsql;


create or replace function pkg_db_util.pg_type_exists(
  p_schema character varying,
  p_typename character varying
)
returns boolean as

$$

declare

begin

    if not exists (
      select
        t.*
      from
        pg_type t
      inner join pg_namespace ns
        on ns.oid = t.typnamespace
      where
        ns.nspname = p_schema and
        t.typname = p_typename
      )
    then

      return false;

    else

      return true;

    end if;

end;

$$

  language plpgsql;


do $$
  begin

    if ((select * from pkg_db_util.pg_type_exists('public', 'competent_relation')) = false) then

      create type public.competent_relation as enum();

    end if;

    alter type public.competent_relation add value if not exists 'user';
    alter type public.competent_relation add value if not exists 'element';
    alter type public.competent_relation add value if not exists 'object';

    comment on type public.competent_relation
        is 'Competent relation type';

  end
$$;



do $$
  begin

    if ((select * from pkg_db_util.pg_type_exists('public', 'organisation_relation')) = false) then

      create type public.organisation_relation as enum();

    end if;

    alter type public.organisation_relation add value if not exists 'department';
    alter type public.organisation_relation add value if not exists 'qualification';

    comment on type public.organisation_relation
        is 'organisation relation type';

  end
$$;


do $$
  begin

    if ((select * from pkg_db_util.pg_type_exists('public', 'work_type')) = false) then

      create type public.work_type as enum();

    end if;

    alter type public.work_type add value if not exists 'Maintenance_0';
    alter type public.work_type add value if not exists 'preventive_control_1';
    alter type public.work_type add value if not exists 'preventive_control';
    alter type public.work_type add value if not exists 'preventive_recovery';
    alter type public.work_type add value if not exists 'adjustment';
    alter type public.work_type add value if not exists 'investigation';

    comment on type public.organisation_relation
        is 'Work type';

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.application_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.application
    (
      application_id integer default nextval('public.application_seq'::regclass) not null
    );

    alter sequence public.application_seq owned by application.application_id;

    alter table public.application add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.application add column if not exists modified timestamp without time zone default null;
    alter table public.application add column if not exists created_by integer default null;
    alter table public.application add column if not exists modified_by integer default null;
    alter table public.application add column if not exists deleted integer default null;
    alter table public.application add column if not exists name character varying;
    alter table public.application alter column name set not null;

    -- Constraints
    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'application', 'pk_application')) = false) then

          alter table public.application add constraint pk_application primary key (application_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers


  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.competent_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.competent
    (
      competent_id integer default nextval('public.competent_seq'::regclass) not null
    );

    alter sequence public.competent_seq owned by competent.competent_id;

    alter table public.competent add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.competent add column if not exists modified timestamp without time zone default null;
    alter table public.competent add column if not exists created_by integer default null;
    alter table public.competent add column if not exists modified_by integer default null;
    alter table public.competent add column if not exists deleted integer default null;
    alter table public.competent add column if not exists name character varying;
    alter table public.competent alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'competent', 'pk_competent')) = false) then

            alter table public.competent add
                constraint pk_competent primary key (competent_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.competent_department_qualification_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.competent_department_qualification_set
    (
      competent_department_qualification_id integer default nextval('public.competent_department_qualification_seq'::regclass) not null
    );

    alter sequence public.competent_department_qualification_seq owned by competent_department_qualification_set.competent_department_qualification_id;

    alter table public.competent_department_qualification_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.competent_department_qualification_set add column if not exists modified timestamp without time zone default null;
    alter table public.competent_department_qualification_set add column if not exists created_by integer default null;
    alter table public.competent_department_qualification_set add column if not exists modified_by integer default null;
    alter table public.competent_department_qualification_set add column if not exists deleted integer default null;
    alter table public.competent_department_qualification_set add column if not exists organisation_id integer;
    alter table public.competent_department_qualification_set alter column organisation_id set not null;
    alter table public.competent_department_qualification_set add column if not exists competent_id integer;
    alter table public.competent_department_qualification_set alter column competent_id set not null;
    alter table public.competent_department_qualification_set add column if not exists department_id integer;
    alter table public.competent_department_qualification_set alter column department_id set not null;
    alter table public.competent_department_qualification_set add column if not exists qualification_id integer;
    alter table public.competent_department_qualification_set alter column qualification_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'competent_department_qualification_set', 'pk_competent_department_qualification')) = false) then

            alter table public.competent_department_qualification_set add
                constraint pk_competent_department_qualification primary key (competent_department_qualification_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.competent_relation_set_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.competent_relation_set
    (
      competent_relation_set_id integer default nextval('public.competent_relation_set_seq'::regclass) not null
    );

    alter sequence public.competent_relation_set_seq owned by competent_relation_set.competent_relation_set_id;

    alter table public.competent_relation_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.competent_relation_set add column if not exists modified timestamp without time zone default null;
    alter table public.competent_relation_set add column if not exists created_by integer default null;
    alter table public.competent_relation_set add column if not exists modified_by integer default null;
    alter table public.competent_relation_set add column if not exists deleted integer default null;
    alter table public.competent_relation_set add column if not exists relation public.competent_relation;
    alter table public.competent_relation_set alter column relation set not null;
    alter table public.competent_relation_set add column if not exists relation_id integer;
    alter table public.competent_relation_set alter column relation_id set not null;
    alter table public.competent_relation_set add column if not exists competent_id integer;
    alter table public.competent_relation_set alter column competent_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'competent_relation_set', 'pk_competent_relation_set')) = false) then

            alter table public.competent_relation_set add
                constraint pk_competent_relation_set primary key (competent_relation_set_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;




do $$
  begin

    -- Sequence

    create sequence if not exists public.department_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.department
    (
      department_id integer default nextval('public.department_seq'::regclass) not null
    );

    alter sequence public.department_seq owned by department.department_id;

    alter table public.department add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.department add column if not exists modified timestamp without time zone default null;
    alter table public.department add column if not exists created_by integer default null;
    alter table public.department add column if not exists modified_by integer default null;
    alter table public.department add column if not exists deleted integer default null;
    alter table public.department add column if not exists name character varying;
    alter table public.department alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'department', 'pk_department')) = false) then

            alter table public.department add
                constraint pk_department primary key (department_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.element_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.element
    (
      element_id integer default nextval('public.element_seq'::regclass) not null
    );

    alter sequence public.element_seq owned by element.element_id;

    alter table public.element add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.element add column if not exists modified timestamp without time zone default null;
    alter table public.element add column if not exists created_by integer default null;
    alter table public.element add column if not exists modified_by integer default null;
    alter table public.element add column if not exists deleted integer default null;
    alter table public.element add column if not exists name character varying;
    alter table public.element alter column name set not null;
    alter table public.element add column if not exists parent_id integer;
    alter table public.element add column if not exists job_id integer;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'element', 'pk_element')) = false) then

          alter table public.element add
            constraint pk_element primary key (element_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.object_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.object
    (
      object_id integer default nextval('public.object_seq'::regclass) not null
    );

    alter sequence public.object_seq owned by object.object_id;

    alter table public.object add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.object add column if not exists modified timestamp without time zone default null;
    alter table public.object add column if not exists created_by integer default null;
    alter table public.object add column if not exists modified_by integer default null;
    alter table public.object add column if not exists deleted integer default null;
    alter table public.object add column if not exists name character varying;
    alter table public.object alter column name set not null;
    alter table public.object add column if not exists parent_id integer;
    alter table public.object add column if not exists element_id integer;
    alter table public.object add column if not exists organisation_id integer;
    alter table public.object alter column organisation_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'object', 'pk_object')) = false) then

          alter table public.object add
              constraint pk_object primary key (object_id);

        end if;

      end
    $constrain$;

    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.organisation_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.organisation
    (
      organisation_id integer default nextval('public.organisation_seq'::regclass) not null
    );

    alter sequence public.organisation_seq owned by organisation.organisation_id;

    alter table public.organisation add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.organisation add column if not exists modified timestamp without time zone default null;
    alter table public.organisation add column if not exists created_by integer default null;
    alter table public.organisation add column if not exists modified_by integer default null;
    alter table public.organisation add column if not exists deleted integer default null;
    alter table public.organisation add column if not exists name character varying;
    alter table public.organisation alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'organisation', 'pk_organisation')) = false) then

          alter table public.organisation add
            constraint pk_organisation primary key (organisation_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.organisation_relation_set_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.organisation_relation_set
    (
      organisation_relation_set_id integer default nextval('public.organisation_relation_set_seq'::regclass) not null
    );

    alter sequence public.organisation_relation_set_seq owned by organisation_relation_set.organisation_relation_set_id;

    alter table public.organisation_relation_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.organisation_relation_set add column if not exists modified timestamp without time zone default null;
    alter table public.organisation_relation_set add column if not exists created_by integer default null;
    alter table public.organisation_relation_set add column if not exists modified_by integer default null;
    alter table public.organisation_relation_set add column if not exists deleted integer default null;
    alter table public.organisation_relation_set add column if not exists relation public.organisation_relation;
    alter table public.organisation_relation_set alter column relation set not null;
    alter table public.organisation_relation_set add column if not exists relation_id integer;
    alter table public.organisation_relation_set alter column relation_id set not null;
    alter table public.organisation_relation_set add column if not exists organisation_id integer;
    alter table public.organisation_relation_set alter column organisation_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'organisation_relation_set', 'pk_organisation_relation_set')) = false) then

          alter table public.organisation_relation_set add
            constraint pk_organisation_relation_set primary key (organisation_relation_set_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.permission_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.permission
    (
      permission_id integer default nextval('public.permission_seq'::regclass) not null
    );

    alter sequence public.permission_seq owned by permission.permission_id;

    alter table public.permission add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.permission add column if not exists modified timestamp without time zone default null;
    alter table public.permission add column if not exists created_by integer default null;
    alter table public.permission add column if not exists modified_by integer default null;
    alter table public.permission add column if not exists deleted integer default null;
    alter table public.permission add column if not exists name character varying;
    alter table public.permission alter column name set not null;
    alter table public.permission add column if not exists alias character varying;
    alter table public.permission alter column alias set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'permission', 'pk_permission')) = false) then

          alter table public.permission add
            constraint pk_permission primary key (permission_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.permission_department_qualification_set_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.permission_department_qualification_set
    (
      permission_department_qualification_set_id integer default nextval('public.permission_department_qualification_set_seq'::regclass) not null
    );

    alter sequence public.permission_department_qualification_set_seq owned by permission_department_qualification_set.permission_department_qualification_set_id;

    alter table public.permission_department_qualification_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.permission_department_qualification_set add column if not exists modified timestamp without time zone default null;
    alter table public.permission_department_qualification_set add column if not exists created_by integer default null;
    alter table public.permission_department_qualification_set add column if not exists modified_by integer default null;
    alter table public.permission_department_qualification_set add column if not exists deleted integer default null;
    alter table public.permission_department_qualification_set add column if not exists organisation_id integer;
    alter table public.permission_department_qualification_set alter column organisation_id set not null;
    alter table public.permission_department_qualification_set add column if not exists permission_id integer;
    alter table public.permission_department_qualification_set alter column permission_id set not null;
    alter table public.permission_department_qualification_set add column if not exists department_id integer;
    alter table public.permission_department_qualification_set alter column department_id set not null;
    alter table public.permission_department_qualification_set add column if not exists qualification_id integer;
    alter table public.permission_department_qualification_set alter column qualification_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'permission_department_qualification_set', 'pk_permission_department_qualification_set')) = false) then

          alter table public.permission_department_qualification_set add
            constraint pk_permission_department_qualification_set primary key (permission_department_qualification_set_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.qualification_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.qualification
    (
      qualification_id integer default nextval('public.qualification_seq'::regclass) not null
    );

    alter sequence public.qualification_seq owned by qualification.qualification_id;

    alter table public.qualification add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.qualification add column if not exists modified timestamp without time zone default null;
    alter table public.qualification add column if not exists created_by integer default null;
    alter table public.qualification add column if not exists modified_by integer default null;
    alter table public.qualification add column if not exists deleted integer default null;
    alter table public.qualification add column if not exists name character varying;
    alter table public.qualification alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'qualification', 'pk_qualification')) = false) then

          alter table public.qualification add
            constraint pk_qualification primary key (qualification_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.session_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.session
    (
      session_id integer default nextval('public.session_seq'::regclass) not null
    );

    alter sequence public.session_seq owned by session.session_id;

    alter table public.session add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.session add column if not exists modified timestamp without time zone default null;
    alter table public.session add column if not exists created_by integer default null;
    alter table public.session add column if not exists modified_by integer default null;
    alter table public.session add column if not exists deleted integer default null;
    alter table public.session add column if not exists user_id integer;
    alter table public.session alter column user_id set not null;
    alter table public.session add column if not exists address character varying;
    alter table public.session add column if not exists client character varying;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'session', 'pk_session')) = false) then

          alter table public.session add
            constraint pk_session primary key (session_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.task_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.task
    (
      task_id integer default nextval('public.task_seq'::regclass) not null
    );

    alter sequence public.task_seq owned by task.task_id;

    alter table public.task add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.task add column if not exists modified timestamp without time zone default null;
    alter table public.task add column if not exists created_by integer default null;
    alter table public.task add column if not exists modified_by integer default null;
    alter table public.task add column if not exists deleted integer default null;

    alter table public.task add column if not exists title character varying;
    alter table public.task alter column title set not null;

    alter table public.task add column if not exists description text;
    alter table public.task alter column description set not null;

    alter table public.task add column if not exists object_id integer;

    alter table public.task add column if not exists accountable_id integer;
    alter table public.task alter column accountable_id set not null;

    alter table public.task add column if not exists assistants integer[];

    alter table public.task add column if not exists application_id integer;

    alter table public.task add column if not exists work_type public.work_type;

    alter table public.task add column if not exists date_to timestamp without time zone default null;
    alter table public.task add column if not exists date_from timestamp without time zone default null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'task', 'pk_task')) = false) then

          alter table public.task add
            constraint pk_task primary key (task_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers


  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.user_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.user
    (
      user_id integer default nextval('public.user_seq'::regclass) not null
    );

    alter sequence public.user_seq owned by "user".user_id;

    alter table public.user add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.user add column if not exists modified timestamp without time zone default null;
    alter table public.user add column if not exists created_by integer default null;
    alter table public.user add column if not exists modified_by integer default null;
    alter table public.user add column if not exists deleted integer default null;
    alter table public.user add column if not exists organisation_id integer default null;
    alter table public.user add column if not exists firstname character varying;
    alter table public.user alter column firstname set not null;
    alter table public.user add column if not exists lastname character varying;
    alter table public.user alter column lastname set not null;
    alter table public.user add column if not exists patronymic character varying;
    alter table public.user alter column patronymic set not null;
    alter table public.user add column if not exists login character varying;
    alter table public.user alter column login set not null;
    alter table public.user add column if not exists email character varying;
    alter table public.user alter column email set not null;
    alter table public.user add column if not exists p_hash character varying;
    alter table public.user alter column p_hash set not null;
    alter table public.user add column if not exists photo character varying;
    alter table public.user add column if not exists department_id integer;
    alter table public.user add column if not exists qualification_id integer;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'user', 'pk_user')) = false) then

          alter table public.user add
            constraint pk_user primary key (user_id);

        end if;

      end
    $constrain$;

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'user', 'uk_user_name')) = false) then

          alter table public.user add
            constraint uk_user_name unique (email, login);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;



create or replace function pkg_auth.user_auth (
  p_login character varying,
  p_p_hash character varying,
  p_address character varying,
  p_client character varying
)

returns integer

as $$

  declare

    v_user_id       integer;
    v_session_id    integer;

  begin

    select
      u.user_id
    into
      v_user_id
    from
      public.user u
    where
      u.deleted is null and
      u.login = p_login and
      u.p_hash = p_p_hash;

    if v_user_id is null then
      raise 'user_not_found';
    end if;

    select
      s.session_id
    into
      v_session_id
    from
      public.session s
    where
      s.deleted is null and
      s.user_id = v_user_id and
      s.address = p_address and
      s.client = p_client;

    raise notice 'v_session_id = %', v_session_id;

    if v_session_id is null then

      insert
        into public.session
          (user_id, address, client)
        values
          (v_user_id, p_address, p_client)
        returning session_id into v_session_id;

    end if;

    return v_session_id;

  end;

$$

language plpgsql;




create or replace function pkg_competent.competent_create(
  p_user_id integer,
  p_competent_name character varying
)

returns integer

as $$

  declare

    v_competent_id      integer;

  begin

    insert into public.competent (
      created_by,
      name
    ) values (
      p_user_id,
      p_competent_name
    ) returning competent_id as v_competent_id;

    return v_competent_id;

  end;

$$

language plpgsql;


create or replace function pkg_competent.competent_list(
  p_competent_id integer[] default null
)

returns table (
  competent_id integer,
  name character varying
)

as $$

  declare

  begin

  return
    query
      select
        p.competent_id,
        p.name
      from
        public.competent p
      where
--         p.deleted is null and
        case
          when
            p_competent_id is not null and array_length(p_competent_id,1) > 0
          then
            p.competent_id = any(p_competent_id)
          else
            true
        end;
  end;
$$

language plpgsql;



create or replace function pkg_competent.competent_relation_list (
  p_relation public.competent_relation,
  p_relation_id integer
)

returns table (
  name character varying,
  available boolean
)

as $$

  declare

  begin

    return
      query
        select
          c.name,
          (
            case
              when
                (
                  select
                    crs.competent_relation_set_id
                  from
                    public.competent_relation_set crs
                  where
                    crs.deleted is null and
                    crs.competent_id = c.competent_id and
                    crs.relation = p_relation and
                    crs.relation_id = p_relation_id
                ) is not null
              then true
              else false
            end
          )::boolean as available
        from
          public.competent c
        where
          c.deleted is null;

  end;

$$

language plpgsql;

-- select * from pkg_competent.competent_relation_list('user', 3);



create or replace function pkg_competent.competent_relation_set (
  p_user_id integer,
  p_competent_id integer[],
  p_relation public.competent_relation,
  p_relation_id integer,
  p_append boolean default true
)

returns void

as $$

declare

    v_competent_id      integer;

begin

  if p_append is false then
    delete
      from
        public.competent_relation_set crs
      where
        crs.relation_id = p_relation_id and
        relation = p_relation;
  end if;

  foreach v_competent_id in array p_competent_id
    loop


      if (
        select
          crs.relation_id
        from
          public.competent_relation_set crs
        where
          crs.competent_id = v_competent_id and
          crs.relation_id = p_relation_id

        ) is null then

          insert into public.competent_relation_set (
            created_by,
            competent_id,
            relation_id,
            relation
          ) values (
            p_user_id,
            v_competent_id,
            p_relation_id,
            p_relation
          );

      end if;

    end loop;

end;

$$

language plpgsql;

-- select pkg_db_util.pg_constraint_exists('public', 'organisation', 'uk_organisation_name')::integer;

create or replace function pkg_db_util.pg_constraint_exists(
  p_schema character varying,
  p_table character varying,
  p_constraint character varying
)
returns boolean as
$$

begin

    if not exists (
      select
        pgc.*
      from
        information_schema.table_constraints pgc
      where
        pgc.table_schema = p_schema and
        pgc.table_name = p_table and
        pgc.constraint_name = p_constraint
      )
    then

      return false;

    else

      return true;

    end if;
end;
$$
  language plpgsql;


create or replace function pkg_db_util.pg_type_exists(
  p_schema character varying,
  p_typename character varying
)
returns boolean as

$$

declare

begin

    if not exists (
      select
        t.*
      from
        pg_type t
      inner join pg_namespace ns
        on ns.oid = t.typnamespace
      where
        ns.nspname = p_schema and
        t.typname = p_typename
      )
    then

      return false;

    else

      return true;

    end if;

end;

$$

  language plpgsql;




create or replace function pkg_department.department_create(
  p_user_id integer,
  p_department_name character varying
)

returns integer

as $$

  declare

    v_department_id      integer;

  begin

    insert into public.department (
      created_by,
      name
    ) values (
      p_user_id,
      p_department_name
    ) returning department_id as v_department_id;

    return v_department_id;

  end;

$$

language plpgsql;

-- drop function if exists pkg_department.department_list(integer[]);

create or replace function pkg_department.department_list (
  p_organisation_id integer,
  p_department_id integer[] default null
)

returns table (
  department_id integer,
  name character varying
)

as $$

declare

begin

  return
    query
      select
        d.department_id,
        d.name

      from
        public.department d

        left join public.organisation_relation_set ors
          on
            ors.deleted is null and
            ors.organisation_id = p_organisation_id and
            ors.relation  = 'department'::public.organisation_relation

      where

        d.deleted is null and
        ors.relation_id = d.department_id and

        case
          when
            p_department_id is not null and
            array_length(p_department_id, 1) > 0
              then d.department_id = any(p_department_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_department.department_list(1)




create or replace function pkg_element.element_create(
  p_user_id integer,
  p_element_name character varying,
  p_parent_id integer default null,
  p_job_id integer default null
)

returns integer

as $$

  declare

    v_element_id      integer;

  begin

    insert into public.element (
      created_by,
      name,
      parent_id,
      job_id
    ) values (
      p_user_id,
      p_element_name,
      p_parent_id,
      p_job_id
    ) returning element_id as v_element_id;

    return v_element_id;

  end;

$$

language plpgsql;

-- drop function if exists pkg_element.element_list(integer[]);

create or replace function pkg_element.element_list (
  p_element_id integer[] default null
)

returns table (
  element_id integer,
  name character varying,
  parent_id integer,
  job_id integer
)

as $$

declare

begin

  return
    query
      select
        e.element_id,
        e.name,
        e.parent_id,
        e.job_id
      from
        public.element e
      where
        deleted is null and
        case
          when
            p_element_id is not null and
            array_length(p_element_id, 1) > 0
              then e.element_id = any(p_element_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_element.element_list()




create or replace function pkg_object.object_create(
  p_user_id integer,
  p_object_name character varying,
  p_organisation_id integer,
  p_parent_id integer default null,
  p_element_id integer default null
)

returns integer

as $$

  declare

    v_object_id      integer;

  begin

    insert into public.object (
      created_by,
      name,
      organisation_id,
      parent_id,
      element_id
    ) values (
      p_user_id,
      p_object_name,
      p_organisation_id,
      p_parent_id,
      p_element_id
    ) returning object_id as v_object_id;

    return v_object_id;

  end;

$$

language plpgsql;

-- drop function if exists pkg_object.object_list(integer[]);

create or replace function pkg_object.object_list (
  p_object_id integer[] default null
)

returns table (
  object_id integer,
  name character varying,
  organisation_id integer,
  parent_id integer,
  element_id integer
)

as $$

declare

begin

  return
    query
      select
        o.object_id,
        o.name,
        o.organisation_id,
        o.parent_id,
        o.element_id
      from
        public.object o
      where
        deleted is null and
        case
          when
            p_object_id is not null and
            array_length(p_object_id, 1) > 0
              then o.object_id = any(p_object_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_object.object_list()


create or replace function pkg_organisation.organisation_check (
  p_organisation_id integer
)

returns boolean

as $$

  declare

    v_organisation_id     integer;

  begin

    select
      o.organisation_id
    into
      v_organisation_id
    from
      public.organisation o
    where
      o.organisation_id = p_organisation_id and
      o.deleted is null;

    if v_organisation_id is null then
      raise 'organisation_id_not_found';
    end if;

    return true;

  end;

$$

language plpgsql;



create or replace function pkg_organisation.organisation_create(
  p_user_id integer,
  p_organisation_name character varying
)

returns integer

as $$

  declare

    v_organisation_id      integer;

  begin

    insert into public.organisation (
      created_by,
      name
    ) values (
      p_user_id,
      p_organisation_name
    ) returning organisation_id into v_organisation_id;

    return v_organisation_id;

  end;

$$

language plpgsql;

-- drop function if exists pkg_organisation.organisation_list(integer[]);

create or replace function pkg_organisation.organisation_list (
  p_organisation_id integer[] default null
)

returns table (
  organisation_id integer,
  name character varying
)

as $$

declare

begin

  return
    query
      select
        o.organisation_id,
        o.name
      from
        public.organisation o
      where
        deleted is null and
        case
          when
            p_organisation_id is not null and
            array_length(p_organisation_id, 1) > 0
              then o.organisation_id = any(p_organisation_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_organisation.organisation_list()


create or replace function pkg_organisation.organisation_relation_set (
  p_user_id integer,
  p_relation public.organisation_relation,
  p_relation_id integer[],
  p_append boolean default true
)

returns void

as $$

  declare

    v_relation_id       integer;
    v_organisation_id   integer;

  begin

    if p_append is false then
      delete
        from
          public.organisation_relation_set ors
        where
          ors.relation_id = p_relation_id and
          ors.relation = p_relation;
    end if;

    select
      u.organisation_id
    into
      v_organisation_id
    from
      public.user u
    where
      u.user_id = p_user_id and
      u.deleted is null;

    if v_organisation_id is null then
      raise 'organisation_id_not_found';
    end if;

    foreach v_relation_id in array p_relation_id
      loop

        if (
          select
            ors.relation_id
          from
            public.organisation_relation_set ors
          where
            ors.relation = p_relation_id and
            ors.relation_id = v_relation_id
        ) is null then

          insert into public.organisation_relation_set (
            created_by,
            relation,
            relation_id,
            organisation_id
          ) values (
            p_user_id,
            p_relation,
            p_relation_id,
            v_organisation_id
          );

        end if;

      end loop;

  end;
$$

language plpgsql;



create or replace function pkg_permission.permission_check(
  p_user_id integer,
  p_permission character varying
)

returns boolean

as $$

  declare

    v_permission_id integer;

  begin

  select
    pdqs.permission_department_qualification_set_id
  into
    v_permission_id
  from
    public.user u

    left join
      public.permission p
      on
--         p.deleted is null and
        p.alias = p_permission

    left join
      public.permission_department_qualification_set pdqs
      on
        pdqs.deleted is null and
        pdqs.permission_id = p.permission_id and
        pdqs.department_id = u.department_id and
        pdqs.qualification_id = u.qualification_id
  where
    u.deleted is null and
    u.user_id = p_user_id;


  if v_permission_id is not null then

    return true;

  else

    return false;

  end if;


  end;

$$

language plpgsql;

-- select * from pkg_permission.permission_check(1, 'qualification.self.read')




create or replace function pkg_permission.permission_create(
  p_user_id integer,
  p_permission_name character varying,
  p_alias character varying
)

returns integer

as $$

  declare

    v_permission_id      integer;

  begin

    raise notice '% %', p_alias, p_permission_name;

    insert into public.permission (
      created_by,
      name,
      alias
    ) values (
      p_user_id,
      p_permission_name,
      p_alias
    )
    returning permission_id into v_permission_id;

    return v_permission_id;

  end;

$$

language plpgsql;


create or replace function pkg_permission.permission_department_qualification_set (
  p_user_id integer,
  p_permission_id integer[],
  p_department_id integer,
  p_qualification_id integer,
  p_append boolean default true
)

returns void

as $$

declare

  v_organisation_id     integer;
  v_permission_id       integer;

begin

  if p_append is false then
    delete
      from
        public.permission_department_qualification_set pdqs
      where
        pdqs.department_id = p_department_id and
        pdqs.qualification_id = p_qualification_id;
  end if;

  select
    u.organisation_id
  into
    v_organisation_id
  from
    public.user u
  where
    u.user_id = p_user_id and
    u.deleted is null;

  if v_organisation_id is null then
    raise 'organisation_id_not_found';
  end if;


  foreach v_permission_id in array p_permission_id
    loop
      if (
        select
          pdqs.permission_id
        from
          public.permission_department_qualification_set pdqs
        where
          pdqs.department_id = p_department_id and
          pdqs.qualification_id = p_qualification_id and
          pdqs.permission_id = v_permission_id

        ) is null then

          insert into public.permission_department_qualification_set (
            created_by,
            department_id,
            qualification_id,
            permission_id,
            organisation_id
          ) values (
            p_user_id,
            p_department_id,
            p_qualification_id,
            v_permission_id,
            v_organisation_id
          );

      end if;

    end loop;

end;

$$

language plpgsql;

create or replace function pkg_permission.permission_get (
  p_action_user_id integer,
  p_department_id integer default null,
  p_qualification_id integer default null
)

returns table (
  department_id integer,
  qualification_id integer,
  permission jsonb[]
)

as $$

  declare

    v_organisation_id      integer;

  begin

    select
      u.organisation_id
    into
      v_organisation_id
    from
      public.user u
    where
      u.user_id = p_action_user_id and
      u.deleted is null;

    if v_organisation_id is null then

      raise 'organisation_not_found';

    end if;


    return
      query
        select
          pdqs.department_id,
          pdqs.qualification_id,
          (
            (
              select
                array_agg(jsonb_build_object(
                  'permission_id', p.permission_id,
                  'name', p.name,
                  'assigned', case when (
                      select
                        pdqs2.permission_id
                      from
                        public.permission_department_qualification_set pdqs2
                      where
                        pdqs2.department_id = pdqs.department_id and
                        pdqs2.qualification_id = pdqs.qualification_id and
                        pdqs2.organisation_id = v_organisation_id and
                        pdqs2.deleted is null and
                        pdqs2.permission_id = p.permission_id
                    ) is null
                    then false
                    else true
                  end
                ))
              from
                public.permission p
              where
                p.permission_id = any((
                  select (array_agg(distinct pdqs2.permission_id))
                  from public.permission_department_qualification_set pdqs2
                  where pdqs2.organisation_id = v_organisation_id
                    and pdqs2.deleted is null
                  group by pdqs2.organisation_id
                )::integer[])
            )
          )

        from
          public.permission_department_qualification_set pdqs
        where
          pdqs.organisation_id = 1 and
          pdqs.deleted is null and

          case
            when p_department_id is not null
              then pdqs.department_id = p_department_id
              else true
            end and

          case
            when p_qualification_id is not null
              then pdqs.qualification_id = p_qualification_id
              else true
            end
        group by pdqs.department_id, pdqs.qualification_id;


  end;
$$

language plpgsql;



create or replace function pkg_permission.permission_list(
  p_permission_id integer[] default null
)

returns table (
  permission_id integer,
  name character varying,
  alias character varying
)

as $$

  declare

  begin

  return
    query
      select
        p.permission_id,
        p.name,
        p.alias
      from
        public.permission p
      where
--         p.deleted is null and
        case
          when
            p_permission_id is not null and array_length(p_permission_id,1) > 0
          then
            p.permission_id = any(p_permission_id)
          else
            true
        end;
  end;
$$

language plpgsql;



create or replace function pkg_permission.user_permission_get (
  p_user_id integer
)

returns table (
  permission_id integer,
  name character varying,
  alias character varying
)

as $$

  declare

  begin

    return
      query
        select
          p.permission_id,
          p.name,
          p.alias
        from
          public.permission p

          left join public.permission_department_qualification_set pdqs
            on
              pdqs.deleted is null

          left join public.user u
            on
              u.deleted is null
        where
          p.deleted is null and
          pdqs.permission_id = p.permission_id and
          pdqs.department_id = u.department_id and
          pdqs.qualification_id = u.qualification_id and
          pdqs.organisation_id = u.organisation_id and
          u.user_id = p_user_id;

  end;

$$

language plpgsql;

-- select * from pkg_permission.user_permission_get(1)





create or replace function pkg_qualification.qualification_create(
  p_user_id integer,
  p_qualification_name character varying
)

returns integer

as $$

  declare

    v_qualification_id      integer;

  begin

    insert into public.qualification (
      created_by,
      name
    ) values (
      p_user_id,
      p_qualification_name
    ) returning qualification_id as v_qualification_id;

    return v_qualification_id;

  end;

$$

language plpgsql;

-- drop function if exists pkg_qualification.qualification_list(integer, integer[]);

create or replace function pkg_qualification.qualification_list (
  p_organisation_id integer,
  p_qualification_id integer[] default null
)

returns table (
  qualification_id integer,
  name character varying
)

as $$

declare

begin

  return
    query
      select
        q.qualification_id,
        q.name
      from
        public.qualification q

        left join public.organisation_relation_set ors
          on
            ors.deleted is null and
            ors.organisation_id = p_organisation_id and
            ors.relation  = 'qualification'::public.organisation_relation
      where
        q.deleted is null and
        ors.relation_id = q.qualification_id and

        case
          when
            p_qualification_id is not null and
            array_length(p_qualification_id, 1) > 0
              then q.qualification_id = any(p_qualification_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_qualification.qualification_list(1)





create or replace function pkg_task.task_create (
    p_user_id integer,
    p_object_id integer,
    p_title character varying,
    p_description text,
    p_accountable_id integer,
    p_assistants integer[] default null,
    p_application_id integer default null,
    p_work_type public.work_type default null,
    p_date_to timestamp without time zone default null,
    p_date_from timestamp without time zone default null
)

returns integer

as $$

  declare

    v_task_id     integer;

  begin

    insert into public.task (
      created_by,
      title,
      description,
      object_id,
      accountable_id,
      assistants,
      application_id,
      work_type,
      date_to,
      date_from
    ) values (
      p_user_id,
      p_title,
      p_description,
      p_object_id,
      p_accountable_id,
      p_assistants,
      p_application_id,
      p_work_type,
      p_date_to,
      p_date_from
    )
    returning task_id into v_task_id;

    return v_task_id;

  end;

$$

language plpgsql;


-- drop function if exists pkg_user.user_create(integer, integer, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer);

create or replace function pkg_user.user_create (
  p_action_user_id integer,
  p_organisation_id integer,
  p_login character varying,
  p_email character varying,
  p_p_hash character varying,
  p_firstname character varying (100),
  p_lastname character varying (100),
  p_patronymic character varying (100) default null,
  p_qualification_id integer default null,
  p_department_id integer default null,
  p_photo character varying default null
)

returns integer

as $$

declare

    v_user_id integer;

begin

    insert into public.user (
        created_by,
        organisation_id,
        firstname,
        lastname,
        patronymic,
        login,
        email,
        p_hash,
        qualification_id,
        department_id,
        photo
    ) values (
        p_action_user_id,
        p_organisation_id,
        p_firstname,
        p_lastname,
        p_patronymic,
        p_login,
        p_email,
        p_p_hash,
        p_qualification_id,
        p_department_id,
        p_photo
    )
    returning user_id into v_user_id;

    return v_user_id;

end;

$$

language plpgsql;



create or replace function pkg_user.user_info_by_session (
  p_session_id integer
)

returns table (
  user_id integer,
  organisation_id integer,
  department_id integer,
  qualification_id integer,
  firstname character varying,
  lastname character varying,
  patronymic character varying,
  email character varying
)

as $$

begin

    return
      query
        select
          u.user_id,
          u.organisation_id,
          u.department_id,
          u.qualification_id,
          u.firstname,
          u.lastname,
          u.patronymic,
          u.email
        from
          public.session s
          left join public.user u
            on s.user_id = u.user_id
        where
          s.session_id = p_session_id
        limit 1;

end;

$$

language plpgsql;

-- drop function if exists pkg_user.user_list(integer, integer[], jsonb);

create or replace function pkg_user.user_list (
  p_organisation integer,
  p_user_id integer[] default null,
  p_filter jsonb default null
)

returns table (
  user_id integer,
  organisation_id integer,
  firstname character varying,
  lastname character varying,
  patronymic character varying,
  email character varying,
  department_id integer,
  department_name character varying,
  qualification_id integer,
  qualification_name character varying,
  photo character varying
)

as $$

declare

begin

  return
    query
      select
        u.user_id,
        u.organisation_id,
        u.firstname,
        u.lastname,
        u.patronymic,
        u.email,
        u.department_id,
        d.name,
        u.qualification_id,
        q.name,
        u.photo
      from
        public.user u
        left join public.department d using(department_id)
        left join public.qualification q using(qualification_id)
      where
        u.deleted is null and
        u.organisation_id = p_organisation and

        case
          when
            p_user_id is not null and
            array_length(p_user_id, 1) > 0
              then u.user_id = any(p_user_id)
              else true
          end and

        case
          when
            p_filter -> 'department_id' is not null
            then u.department_id = (p_filter -> 'department_id')::integer
            else true
          end and

        case
          when
            p_filter -> 'qualification_id' is not null
            then u.department_id = (p_filter -> 'qualification_id')::integer
            else true
          end;

end;

$$

language plpgsql;


do $$
  begin

    -- Sequence

    create sequence if not exists public.application_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.application
    (
      application_id integer default nextval('public.application_seq'::regclass) not null
    );

    alter sequence public.application_seq owned by application.application_id;

    alter table public.application add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.application add column if not exists modified timestamp without time zone default null;
    alter table public.application add column if not exists created_by integer default null;
    alter table public.application add column if not exists modified_by integer default null;
    alter table public.application add column if not exists deleted integer default null;
    alter table public.application add column if not exists name character varying;
    alter table public.application alter column name set not null;

    -- Constraints
    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'application', 'pk_application')) = false) then

          alter table public.application add constraint pk_application primary key (application_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers


  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.competent_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.competent
    (
      competent_id integer default nextval('public.competent_seq'::regclass) not null
    );

    alter sequence public.competent_seq owned by competent.competent_id;

    alter table public.competent add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.competent add column if not exists modified timestamp without time zone default null;
    alter table public.competent add column if not exists created_by integer default null;
    alter table public.competent add column if not exists modified_by integer default null;
    alter table public.competent add column if not exists deleted integer default null;
    alter table public.competent add column if not exists name character varying;
    alter table public.competent alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'competent', 'pk_competent')) = false) then

            alter table public.competent add
                constraint pk_competent primary key (competent_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.competent_department_qualification_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.competent_department_qualification_set
    (
      competent_department_qualification_id integer default nextval('public.competent_department_qualification_seq'::regclass) not null
    );

    alter sequence public.competent_department_qualification_seq owned by competent_department_qualification_set.competent_department_qualification_id;

    alter table public.competent_department_qualification_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.competent_department_qualification_set add column if not exists modified timestamp without time zone default null;
    alter table public.competent_department_qualification_set add column if not exists created_by integer default null;
    alter table public.competent_department_qualification_set add column if not exists modified_by integer default null;
    alter table public.competent_department_qualification_set add column if not exists deleted integer default null;
    alter table public.competent_department_qualification_set add column if not exists organisation_id integer;
    alter table public.competent_department_qualification_set alter column organisation_id set not null;
    alter table public.competent_department_qualification_set add column if not exists competent_id integer;
    alter table public.competent_department_qualification_set alter column competent_id set not null;
    alter table public.competent_department_qualification_set add column if not exists department_id integer;
    alter table public.competent_department_qualification_set alter column department_id set not null;
    alter table public.competent_department_qualification_set add column if not exists qualification_id integer;
    alter table public.competent_department_qualification_set alter column qualification_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'competent_department_qualification_set', 'pk_competent_department_qualification')) = false) then

            alter table public.competent_department_qualification_set add
                constraint pk_competent_department_qualification primary key (competent_department_qualification_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.competent_relation_set_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.competent_relation_set
    (
      competent_relation_set_id integer default nextval('public.competent_relation_set_seq'::regclass) not null
    );

    alter sequence public.competent_relation_set_seq owned by competent_relation_set.competent_relation_set_id;

    alter table public.competent_relation_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.competent_relation_set add column if not exists modified timestamp without time zone default null;
    alter table public.competent_relation_set add column if not exists created_by integer default null;
    alter table public.competent_relation_set add column if not exists modified_by integer default null;
    alter table public.competent_relation_set add column if not exists deleted integer default null;
    alter table public.competent_relation_set add column if not exists relation public.competent_relation;
    alter table public.competent_relation_set alter column relation set not null;
    alter table public.competent_relation_set add column if not exists relation_id integer;
    alter table public.competent_relation_set alter column relation_id set not null;
    alter table public.competent_relation_set add column if not exists competent_id integer;
    alter table public.competent_relation_set alter column competent_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'competent_relation_set', 'pk_competent_relation_set')) = false) then

            alter table public.competent_relation_set add
                constraint pk_competent_relation_set primary key (competent_relation_set_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;




do $$
  begin

    -- Sequence

    create sequence if not exists public.department_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.department
    (
      department_id integer default nextval('public.department_seq'::regclass) not null
    );

    alter sequence public.department_seq owned by department.department_id;

    alter table public.department add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.department add column if not exists modified timestamp without time zone default null;
    alter table public.department add column if not exists created_by integer default null;
    alter table public.department add column if not exists modified_by integer default null;
    alter table public.department add column if not exists deleted integer default null;
    alter table public.department add column if not exists name character varying;
    alter table public.department alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'department', 'pk_department')) = false) then

            alter table public.department add
                constraint pk_department primary key (department_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.element_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.element
    (
      element_id integer default nextval('public.element_seq'::regclass) not null
    );

    alter sequence public.element_seq owned by element.element_id;

    alter table public.element add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.element add column if not exists modified timestamp without time zone default null;
    alter table public.element add column if not exists created_by integer default null;
    alter table public.element add column if not exists modified_by integer default null;
    alter table public.element add column if not exists deleted integer default null;
    alter table public.element add column if not exists name character varying;
    alter table public.element alter column name set not null;
    alter table public.element add column if not exists parent_id integer;
    alter table public.element add column if not exists job_id integer;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'element', 'pk_element')) = false) then

          alter table public.element add
            constraint pk_element primary key (element_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.object_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.object
    (
      object_id integer default nextval('public.object_seq'::regclass) not null
    );

    alter sequence public.object_seq owned by object.object_id;

    alter table public.object add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.object add column if not exists modified timestamp without time zone default null;
    alter table public.object add column if not exists created_by integer default null;
    alter table public.object add column if not exists modified_by integer default null;
    alter table public.object add column if not exists deleted integer default null;
    alter table public.object add column if not exists name character varying;
    alter table public.object alter column name set not null;
    alter table public.object add column if not exists parent_id integer;
    alter table public.object add column if not exists element_id integer;
    alter table public.object add column if not exists organisation_id integer;
    alter table public.object alter column organisation_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'object', 'pk_object')) = false) then

          alter table public.object add
              constraint pk_object primary key (object_id);

        end if;

      end
    $constrain$;

    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.organisation_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.organisation
    (
      organisation_id integer default nextval('public.organisation_seq'::regclass) not null
    );

    alter sequence public.organisation_seq owned by organisation.organisation_id;

    alter table public.organisation add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.organisation add column if not exists modified timestamp without time zone default null;
    alter table public.organisation add column if not exists created_by integer default null;
    alter table public.organisation add column if not exists modified_by integer default null;
    alter table public.organisation add column if not exists deleted integer default null;
    alter table public.organisation add column if not exists name character varying;
    alter table public.organisation alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'organisation', 'pk_organisation')) = false) then

          alter table public.organisation add
            constraint pk_organisation primary key (organisation_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.organisation_relation_set_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.organisation_relation_set
    (
      organisation_relation_set_id integer default nextval('public.organisation_relation_set_seq'::regclass) not null
    );

    alter sequence public.organisation_relation_set_seq owned by organisation_relation_set.organisation_relation_set_id;

    alter table public.organisation_relation_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.organisation_relation_set add column if not exists modified timestamp without time zone default null;
    alter table public.organisation_relation_set add column if not exists created_by integer default null;
    alter table public.organisation_relation_set add column if not exists modified_by integer default null;
    alter table public.organisation_relation_set add column if not exists deleted integer default null;
    alter table public.organisation_relation_set add column if not exists relation public.organisation_relation;
    alter table public.organisation_relation_set alter column relation set not null;
    alter table public.organisation_relation_set add column if not exists relation_id integer;
    alter table public.organisation_relation_set alter column relation_id set not null;
    alter table public.organisation_relation_set add column if not exists organisation_id integer;
    alter table public.organisation_relation_set alter column organisation_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'organisation_relation_set', 'pk_organisation_relation_set')) = false) then

          alter table public.organisation_relation_set add
            constraint pk_organisation_relation_set primary key (organisation_relation_set_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.permission_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.permission
    (
      permission_id integer default nextval('public.permission_seq'::regclass) not null
    );

    alter sequence public.permission_seq owned by permission.permission_id;

    alter table public.permission add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.permission add column if not exists modified timestamp without time zone default null;
    alter table public.permission add column if not exists created_by integer default null;
    alter table public.permission add column if not exists modified_by integer default null;
    alter table public.permission add column if not exists deleted integer default null;
    alter table public.permission add column if not exists name character varying;
    alter table public.permission alter column name set not null;
    alter table public.permission add column if not exists alias character varying;
    alter table public.permission alter column alias set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'permission', 'pk_permission')) = false) then

          alter table public.permission add
            constraint pk_permission primary key (permission_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.permission_department_qualification_set_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.permission_department_qualification_set
    (
      permission_department_qualification_set_id integer default nextval('public.permission_department_qualification_set_seq'::regclass) not null
    );

    alter sequence public.permission_department_qualification_set_seq owned by permission_department_qualification_set.permission_department_qualification_set_id;

    alter table public.permission_department_qualification_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.permission_department_qualification_set add column if not exists modified timestamp without time zone default null;
    alter table public.permission_department_qualification_set add column if not exists created_by integer default null;
    alter table public.permission_department_qualification_set add column if not exists modified_by integer default null;
    alter table public.permission_department_qualification_set add column if not exists deleted integer default null;
    alter table public.permission_department_qualification_set add column if not exists organisation_id integer;
    alter table public.permission_department_qualification_set alter column organisation_id set not null;
    alter table public.permission_department_qualification_set add column if not exists permission_id integer;
    alter table public.permission_department_qualification_set alter column permission_id set not null;
    alter table public.permission_department_qualification_set add column if not exists department_id integer;
    alter table public.permission_department_qualification_set alter column department_id set not null;
    alter table public.permission_department_qualification_set add column if not exists qualification_id integer;
    alter table public.permission_department_qualification_set alter column qualification_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'permission_department_qualification_set', 'pk_permission_department_qualification_set')) = false) then

          alter table public.permission_department_qualification_set add
            constraint pk_permission_department_qualification_set primary key (permission_department_qualification_set_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.qualification_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.qualification
    (
      qualification_id integer default nextval('public.qualification_seq'::regclass) not null
    );

    alter sequence public.qualification_seq owned by qualification.qualification_id;

    alter table public.qualification add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.qualification add column if not exists modified timestamp without time zone default null;
    alter table public.qualification add column if not exists created_by integer default null;
    alter table public.qualification add column if not exists modified_by integer default null;
    alter table public.qualification add column if not exists deleted integer default null;
    alter table public.qualification add column if not exists name character varying;
    alter table public.qualification alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'qualification', 'pk_qualification')) = false) then

          alter table public.qualification add
            constraint pk_qualification primary key (qualification_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.session_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.session
    (
      session_id integer default nextval('public.session_seq'::regclass) not null
    );

    alter sequence public.session_seq owned by session.session_id;

    alter table public.session add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.session add column if not exists modified timestamp without time zone default null;
    alter table public.session add column if not exists created_by integer default null;
    alter table public.session add column if not exists modified_by integer default null;
    alter table public.session add column if not exists deleted integer default null;
    alter table public.session add column if not exists user_id integer;
    alter table public.session alter column user_id set not null;
    alter table public.session add column if not exists address character varying;
    alter table public.session add column if not exists client character varying;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'session', 'pk_session')) = false) then

          alter table public.session add
            constraint pk_session primary key (session_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.task_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.task
    (
      task_id integer default nextval('public.task_seq'::regclass) not null
    );

    alter sequence public.task_seq owned by task.task_id;

    alter table public.task add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.task add column if not exists modified timestamp without time zone default null;
    alter table public.task add column if not exists created_by integer default null;
    alter table public.task add column if not exists modified_by integer default null;
    alter table public.task add column if not exists deleted integer default null;

    alter table public.task add column if not exists title character varying;
    alter table public.task alter column title set not null;

    alter table public.task add column if not exists description text;
    alter table public.task alter column description set not null;

    alter table public.task add column if not exists object_id integer;

    alter table public.task add column if not exists accountable_id integer;
    alter table public.task alter column accountable_id set not null;

    alter table public.task add column if not exists assistants integer[];

    alter table public.task add column if not exists application_id integer;

    alter table public.task add column if not exists work_type public.work_type;

    alter table public.task add column if not exists date_to timestamp without time zone default null;
    alter table public.task add column if not exists date_from timestamp without time zone default null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'task', 'pk_task')) = false) then

          alter table public.task add
            constraint pk_task primary key (task_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers


  end
$$;


do $$
  begin

    -- Sequence

    create sequence if not exists public.user_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.user
    (
      user_id integer default nextval('public.user_seq'::regclass) not null
    );

    alter sequence public.user_seq owned by "user".user_id;

    alter table public.user add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.user add column if not exists modified timestamp without time zone default null;
    alter table public.user add column if not exists created_by integer default null;
    alter table public.user add column if not exists modified_by integer default null;
    alter table public.user add column if not exists deleted integer default null;
    alter table public.user add column if not exists organisation_id integer default null;
    alter table public.user add column if not exists firstname character varying;
    alter table public.user alter column firstname set not null;
    alter table public.user add column if not exists lastname character varying;
    alter table public.user alter column lastname set not null;
    alter table public.user add column if not exists patronymic character varying;
    alter table public.user alter column patronymic set not null;
    alter table public.user add column if not exists login character varying;
    alter table public.user alter column login set not null;
    alter table public.user add column if not exists email character varying;
    alter table public.user alter column email set not null;
    alter table public.user add column if not exists p_hash character varying;
    alter table public.user alter column p_hash set not null;
    alter table public.user add column if not exists photo character varying;
    alter table public.user add column if not exists department_id integer;
    alter table public.user add column if not exists qualification_id integer;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'user', 'pk_user')) = false) then

          alter table public.user add
            constraint pk_user primary key (user_id);

        end if;

      end
    $constrain$;

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'user', 'uk_user_name')) = false) then

          alter table public.user add
            constraint uk_user_name unique (email, login);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;


do $$
  begin

    insert into
      public.competent
        (name)
      values
        ('ВК ТТ'),
        ('ВК ТТ-10кВ и ниже'),
        ('ВК ТН'),
        ('ВК ТН-10кВ и ниже'),
        ('ВК В-220кВ и выше')

    on conflict do nothing;

  end
$$;

do $$
  begin

    insert into
      public.department
        (name)
      values

          ('Отдел РЗА и АСУ ТП'),
          ('Отдел кадров')

    on conflict do nothing;

  end
$$;

do $$
  begin

    insert into
      public.permission
        (name, alias)
      values

        ('Просмотр своей квалификации','qualification.self.read'),
        ('Просмотр квалификации','qualification.read'),
        ('Просмотр пользователй','user.read'),
        ('Просмотр информации о себе','user.self.info.read'),
        ('Просмотр компетенций','competent.read'),

        ('Просмотр своих компетенций','competent.user.self.read'),
        ('Установить компетенции сущности','competent.relation.write'),
        ('Просмотр компетенции сущности','competent.relation.read'),
        ('Просмотр списка объектов','object.read'),
        ('Изменять объекты','object.write'),

        ('Просмотр списка элементов','element.read'),
        ('Изменение элементов','element.write'),
        ('Список прав','permission.read'),
        ('Изменить право','permission.write'),
        ('Список служб','department.read'),

        ('Создать/изменить пользователя','user.write')

    on conflict do nothing;

  end
$$;

do $$
  begin

    insert into
      public.qualification
        (name)
      values

          ('Инженер'),
          ('Инженер 2 категории'),
          ('Специалист'),
          ('Специалист 2 категории')

    on conflict do nothing;

  end
$$;

do $$
  begin

    insert into
      public.user
        (created_by, organisation_id, firstname, lastname, patronymic, login, email, p_hash)
      values

        (-1, 1, 'ИнженерРЗА', 'Ф', 'О', 'rzarza', 'rza@mail.ru', '111111'),
        (-1, 1, 'ОткделКадров', 'Ф', 'О', 'otdotd', 'otd@mail.ru', '111111')

    on conflict do nothing;

  end
$$;