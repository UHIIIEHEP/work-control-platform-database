do $$
  begin

    insert into
      public.qualification
        (name)
      values

          ('Инженер'),
          ('Инженер 2 категории'),
          ('Специалист'),
          ('Специалист 2 категории')

    on conflict do nothing;

  end
$$;