

create or replace function pkg_qualification.qualification_create(
  p_user_id integer,
  p_qualification_name character varying
)

returns integer

as $$

  declare

    v_qualification_id      integer;

  begin

    insert into public.qualification (
      created_by,
      name
    ) values (
      p_user_id,
      p_qualification_name
    ) returning qualification_id as v_qualification_id;

    return v_qualification_id;

  end;

$$

language plpgsql;