-- drop function if exists pkg_qualification.qualification_list(integer, integer[]);

create or replace function pkg_qualification.qualification_list (
  p_organisation_id integer,
  p_qualification_id integer[] default null
)

returns table (
  qualification_id integer,
  name character varying
)

as $$

declare

begin

  return
    query
      select
        q.qualification_id,
        q.name
      from
        public.qualification q

        left join public.organisation_relation_set ors
          on
            ors.deleted is null and
            ors.organisation_id = p_organisation_id and
            ors.relation  = 'qualification'::public.organisation_relation
      where
        q.deleted is null and
        ors.relation_id = q.qualification_id and

        case
          when
            p_qualification_id is not null and
            array_length(p_qualification_id, 1) > 0
              then q.qualification_id = any(p_qualification_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_qualification.qualification_list(1)
