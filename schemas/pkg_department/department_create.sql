

create or replace function pkg_department.department_create(
  p_user_id integer,
  p_department_name character varying
)

returns integer

as $$

  declare

    v_department_id      integer;

  begin

    insert into public.department (
      created_by,
      name
    ) values (
      p_user_id,
      p_department_name
    ) returning department_id as v_department_id;

    return v_department_id;

  end;

$$

language plpgsql;