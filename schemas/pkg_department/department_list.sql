-- drop function if exists pkg_department.department_list(integer[]);

create or replace function pkg_department.department_list (
  p_organisation_id integer,
  p_department_id integer[] default null
)

returns table (
  department_id integer,
  name character varying
)

as $$

declare

begin

  return
    query
      select
        d.department_id,
        d.name

      from
        public.department d

        left join public.organisation_relation_set ors
          on
            ors.deleted is null and
            ors.organisation_id = p_organisation_id and
            ors.relation  = 'department'::public.organisation_relation

      where

        d.deleted is null and
        ors.relation_id = d.department_id and

        case
          when
            p_department_id is not null and
            array_length(p_department_id, 1) > 0
              then d.department_id = any(p_department_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_department.department_list(1)
