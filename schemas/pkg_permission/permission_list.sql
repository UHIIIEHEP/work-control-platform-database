
create or replace function pkg_permission.permission_list(
  p_permission_id integer[] default null
)

returns table (
  permission_id integer,
  name character varying,
  alias character varying
)

as $$

  declare

  begin

  return
    query
      select
        p.permission_id,
        p.name,
        p.alias
      from
        public.permission p
      where
--         p.deleted is null and
        case
          when
            p_permission_id is not null and array_length(p_permission_id,1) > 0
          then
            p.permission_id = any(p_permission_id)
          else
            true
        end;
  end;
$$

language plpgsql;
