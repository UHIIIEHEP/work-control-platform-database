
create or replace function pkg_permission.permission_department_qualification_set (
  p_user_id integer,
  p_permission_id integer[],
  p_department_id integer,
  p_qualification_id integer,
  p_append boolean default true
)

returns void

as $$

declare

  v_organisation_id     integer;
  v_permission_id       integer;

begin

  if p_append is false then
    delete
      from
        public.permission_department_qualification_set pdqs
      where
        pdqs.department_id = p_department_id and
        pdqs.qualification_id = p_qualification_id;
  end if;

  select
    u.organisation_id
  into
    v_organisation_id
  from
    public.user u
  where
    u.user_id = p_user_id and
    u.deleted is null;

  if v_organisation_id is null then
    raise 'organisation_id_not_found';
  end if;


  foreach v_permission_id in array p_permission_id
    loop
      if (
        select
          pdqs.permission_id
        from
          public.permission_department_qualification_set pdqs
        where
          pdqs.department_id = p_department_id and
          pdqs.qualification_id = p_qualification_id and
          pdqs.permission_id = v_permission_id

        ) is null then

          insert into public.permission_department_qualification_set (
            created_by,
            department_id,
            qualification_id,
            permission_id,
            organisation_id
          ) values (
            p_user_id,
            p_department_id,
            p_qualification_id,
            v_permission_id,
            v_organisation_id
          );

      end if;

    end loop;

end;

$$

language plpgsql;