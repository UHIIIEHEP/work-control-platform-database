

create or replace function pkg_permission.permission_create(
  p_user_id integer,
  p_permission_name character varying,
  p_alias character varying
)

returns integer

as $$

  declare

    v_permission_id      integer;

  begin

    raise notice '% %', p_alias, p_permission_name;

    insert into public.permission (
      created_by,
      name,
      alias
    ) values (
      p_user_id,
      p_permission_name,
      p_alias
    )
    returning permission_id into v_permission_id;

    return v_permission_id;

  end;

$$

language plpgsql;