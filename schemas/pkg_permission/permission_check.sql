

create or replace function pkg_permission.permission_check(
  p_user_id integer,
  p_permission character varying
)

returns boolean

as $$

  declare

    v_permission_id integer;

  begin

  select
    pdqs.permission_department_qualification_set_id
  into
    v_permission_id
  from
    public.user u

    left join
      public.permission p
      on
--         p.deleted is null and
        p.alias = p_permission

    left join
      public.permission_department_qualification_set pdqs
      on
        pdqs.deleted is null and
        pdqs.permission_id = p.permission_id and
        pdqs.department_id = u.department_id and
        pdqs.qualification_id = u.qualification_id
  where
    u.deleted is null and
    u.user_id = p_user_id;


  if v_permission_id is not null then

    return true;

  else

    return false;

  end if;

  end;

$$

language plpgsql;

-- select * from pkg_permission.permission_check(1, 'qualification.self.read')
