do $$
  begin

    insert into
      public.permission
        (name, alias)
      values

        ('Просмотр своей квалификации','qualification.self.read'),
        ('Просмотр квалификации','qualification.read'),
        ('Просмотр пользователй','user.read'),
        ('Просмотр информации о себе','user.self.info.read'),
        ('Просмотр компетенций','competent.read'),

        ('Просмотр своих компетенций','competent.user.self.read'),
        ('Установить компетенции сущности','competent.relation.write'),
        ('Просмотр компетенции сущности','competent.relation.read'),
        ('Просмотр списка объектов','object.read'),
        ('Изменять объекты','object.write'),

        ('Просмотр списка элементов','element.read'),
        ('Изменение элементов','element.write'),
        ('Список прав','permission.read'),
        ('Изменить право','permission.write'),
        ('Список служб','department.read'),

        ('Создать/изменить пользователя','user.write')

    on conflict do nothing;

  end
$$;