
create or replace function pkg_permission.user_permission_get (
  p_user_id integer
)

returns table (
  permission_id integer,
  name character varying,
  alias character varying
)

as $$

  declare

  begin

    return
      query
        select
          p.permission_id,
          p.name,
          p.alias
        from
          public.permission p

          left join public.permission_department_qualification_set pdqs
            on
              pdqs.deleted is null

          left join public.user u
            on
              u.deleted is null
        where
          p.deleted is null and
          pdqs.permission_id = p.permission_id and
          pdqs.department_id = u.department_id and
          pdqs.qualification_id = u.qualification_id and
          pdqs.organisation_id = u.organisation_id and
          u.user_id = p_user_id;

  end;

$$

language plpgsql;

-- select * from pkg_permission.user_permission_get(1)

