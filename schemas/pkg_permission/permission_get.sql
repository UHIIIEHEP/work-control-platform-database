create or replace function pkg_permission.permission_get (
  p_action_user_id integer,
  p_department_id integer default null,
  p_qualification_id integer default null
)

returns table (
  department_id integer,
  qualification_id integer,
  permission jsonb[]
)

as $$

  declare

    v_organisation_id      integer;

  begin

    select
      u.organisation_id
    into
      v_organisation_id
    from
      public.user u
    where
      u.user_id = p_action_user_id and
      u.deleted is null;

    if v_organisation_id is null then

      raise 'organisation_not_found';

    end if;


    return
      query
        select
          pdqs.department_id,
          pdqs.qualification_id,
          (
            (
              select
                array_agg(jsonb_build_object(
                  'permission_id', p.permission_id,
                  'assigned', case when (
                      select
                        pdqs2.permission_id
                      from
                        public.permission_department_qualification_set pdqs2
                      where
                        pdqs2.department_id = pdqs.department_id and
                        pdqs2.qualification_id = pdqs.qualification_id and
                        pdqs2.organisation_id = v_organisation_id and
                        pdqs2.deleted is null and
                        pdqs2.permission_id = p.permission_id
                    ) is null
                    then false
                    else true
                  end
                ))
              from
                public.permission p
              where
                p.permission_id = any((
                  select (array_agg(distinct pdqs2.permission_id))
                  from public.permission_department_qualification_set pdqs2
                  where pdqs2.organisation_id = v_organisation_id
                    and pdqs2.deleted is null
                  group by pdqs2.organisation_id
                )::integer[])
            )
          )

        from
          public.permission_department_qualification_set pdqs
        where
          pdqs.organisation_id = v_organisation_id and
          pdqs.deleted is null and

          case
            when p_department_id is not null
              then pdqs.department_id = p_department_id
              else true
            end and

          case
            when p_qualification_id is not null
              then pdqs.qualification_id = p_qualification_id
              else true
            end
        group by pdqs.department_id, pdqs.qualification_id;


  end;
$$

language plpgsql;
