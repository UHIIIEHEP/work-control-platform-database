-- select pkg_db_util.pg_constraint_exists('public', 'organisation', 'uk_organisation_name')::integer;

create or replace function pkg_db_util.pg_constraint_exists(
  p_schema character varying,
  p_table character varying,
  p_constraint character varying
)
returns boolean as
$$

begin

    if not exists (
      select
        pgc.*
      from
        information_schema.table_constraints pgc
      where
        pgc.table_schema = p_schema and
        pgc.table_name = p_table and
        pgc.constraint_name = p_constraint
      )
    then

      return false;

    else

      return true;

    end if;
end;
$$
  language plpgsql;
