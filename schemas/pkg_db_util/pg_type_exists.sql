create or replace function pkg_db_util.pg_type_exists(
  p_schema character varying,
  p_typename character varying
)
returns boolean as

$$

declare

begin

    if not exists (
      select
        t.*
      from
        pg_type t
      inner join pg_namespace ns
        on ns.oid = t.typnamespace
      where
        ns.nspname = p_schema and
        t.typname = p_typename
      )
    then

      return false;

    else

      return true;

    end if;

end;

$$

  language plpgsql;
