

create or replace function pkg_element.element_create(
  p_user_id integer,
  p_element_name character varying,
  p_parent_id integer default null,
  p_job_id integer default null
)

returns integer

as $$

  declare

    v_element_id      integer;

  begin

    insert into public.element (
      created_by,
      name,
      parent_id,
      job_id
    ) values (
      p_user_id,
      p_element_name,
      p_parent_id,
      p_job_id
    ) returning element_id as v_element_id;

    return v_element_id;

  end;

$$

language plpgsql;