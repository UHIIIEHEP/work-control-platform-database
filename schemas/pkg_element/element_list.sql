-- drop function if exists pkg_element.element_list(integer[]);

create or replace function pkg_element.element_list (
  p_element_id integer[] default null
)

returns table (
  element_id integer,
  name character varying,
  parent_id integer,
  job_id integer
)

as $$

declare

begin

  return
    query
      select
        e.element_id,
        e.name,
        e.parent_id,
        e.job_id
      from
        public.element e
      where
        deleted is null and
        case
          when
            p_element_id is not null and
            array_length(p_element_id, 1) > 0
              then e.element_id = any(p_element_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_element.element_list()
