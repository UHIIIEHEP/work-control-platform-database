

create or replace function pkg_object.object_create(
  p_user_id integer,
  p_object_name character varying,
  p_organisation_id integer,
  p_parent_id integer default null,
  p_element_id integer default null
)

returns integer

as $$

  declare

    v_object_id      integer;

  begin

    insert into public.object (
      created_by,
      name,
      organisation_id,
      parent_id,
      element_id
    ) values (
      p_user_id,
      p_object_name,
      p_organisation_id,
      p_parent_id,
      p_element_id
    ) returning object_id as v_object_id;

    return v_object_id;

  end;

$$

language plpgsql;