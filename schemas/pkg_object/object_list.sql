-- drop function if exists pkg_object.object_list(integer[]);

create or replace function pkg_object.object_list (
  p_object_id integer[] default null
)

returns table (
  object_id integer,
  name character varying,
  organisation_id integer,
  parent_id integer,
  element_id integer
)

as $$

declare

begin

  return
    query
      select
        o.object_id,
        o.name,
        o.organisation_id,
        o.parent_id,
        o.element_id
      from
        public.object o
      where
        deleted is null and
        case
          when
            p_object_id is not null and
            array_length(p_object_id, 1) > 0
              then o.object_id = any(p_object_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_object.object_list()
