
create or replace function pkg_auth.user_auth (
  p_login character varying,
  p_p_hash character varying,
  p_address character varying,
  p_client character varying
)

returns integer

as $$

  declare

    v_user_id       integer;
    v_session_id    integer;

  begin

    select
      u.user_id
    into
      v_user_id
    from
      public.user u
    where
      u.deleted is null and
      u.login = p_login and
      u.p_hash = p_p_hash;

    if v_user_id is null then
      raise 'user_not_found';
    end if;

    select
      s.session_id
    into
      v_session_id
    from
      public.session s
    where
      s.deleted is null and
      s.user_id = v_user_id and
      s.address = p_address and
      s.client = p_client;

    raise notice 'v_session_id = %', v_session_id;

    if v_session_id is null then

      insert
        into public.session
          (user_id, address, client)
        values
          (v_user_id, p_address, p_client)
        returning session_id into v_session_id;

    end if;

    return v_session_id;

  end;

$$

language plpgsql;
