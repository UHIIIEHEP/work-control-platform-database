
create or replace function pkg_competent.competent_relation_set (
  p_user_id integer,
  p_competent_id integer[],
  p_relation public.competent_relation,
  p_relation_id integer,
  p_append boolean default true
)

returns void

as $$

declare

    v_competent_id      integer;

begin

  if p_append is false then
    delete
      from
        public.competent_relation_set crs
      where
        crs.relation_id = p_relation_id and
        relation = p_relation;
  end if;

  foreach v_competent_id in array p_competent_id
    loop


      if (
        select
          crs.relation_id
        from
          public.competent_relation_set crs
        where
          crs.competent_id = v_competent_id and
          crs.relation_id = p_relation_id

        ) is null then

          insert into public.competent_relation_set (
            created_by,
            competent_id,
            relation_id,
            relation
          ) values (
            p_user_id,
            v_competent_id,
            p_relation_id,
            p_relation
          );

      end if;

    end loop;

end;

$$

language plpgsql;