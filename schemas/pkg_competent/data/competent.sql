do $$
  begin

    insert into
      public.competent
        (name)
      values
        ('ВК ТТ'),
        ('ВК ТТ-10кВ и ниже'),
        ('ВК ТН'),
        ('ВК ТН-10кВ и ниже'),
        ('ВК В-220кВ и выше')

    on conflict do nothing;

  end
$$;