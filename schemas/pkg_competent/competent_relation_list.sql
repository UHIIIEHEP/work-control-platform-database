
create or replace function pkg_competent.competent_relation_list (
  p_relation public.competent_relation,
  p_relation_id integer
)

returns table (
  name character varying,
  available boolean
)

as $$

  declare

  begin

    return
      query
        select
          c.name,
          (
            case
              when
                (
                  select
                    crs.competent_relation_set_id
                  from
                    public.competent_relation_set crs
                  where
                    crs.deleted is null and
                    crs.competent_id = c.competent_id and
                    crs.relation = p_relation and
                    crs.relation_id = p_relation_id
                ) is not null
              then true
              else false
            end
          )::boolean as available
        from
          public.competent c
        where
          c.deleted is null;

  end;

$$

language plpgsql;

-- select * from pkg_competent.competent_relation_list('user', 3);
