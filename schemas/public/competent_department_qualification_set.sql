do $$
  begin

    -- Sequence

    create sequence if not exists public.competent_department_qualification_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.competent_department_qualification_set
    (
      competent_department_qualification_id integer default nextval('public.competent_department_qualification_seq'::regclass) not null
    );

    alter sequence public.competent_department_qualification_seq owned by competent_department_qualification_set.competent_department_qualification_id;

    alter table public.competent_department_qualification_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.competent_department_qualification_set add column if not exists modified timestamp without time zone default null;
    alter table public.competent_department_qualification_set add column if not exists created_by integer default null;
    alter table public.competent_department_qualification_set add column if not exists modified_by integer default null;
    alter table public.competent_department_qualification_set add column if not exists deleted integer default null;
    alter table public.competent_department_qualification_set add column if not exists organisation_id integer;
    alter table public.competent_department_qualification_set alter column organisation_id set not null;
    alter table public.competent_department_qualification_set add column if not exists competent_id integer;
    alter table public.competent_department_qualification_set alter column competent_id set not null;
    alter table public.competent_department_qualification_set add column if not exists department_id integer;
    alter table public.competent_department_qualification_set alter column department_id set not null;
    alter table public.competent_department_qualification_set add column if not exists qualification_id integer;
    alter table public.competent_department_qualification_set alter column qualification_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'competent_department_qualification_set', 'pk_competent_department_qualification')) = false) then

            alter table public.competent_department_qualification_set add
                constraint pk_competent_department_qualification primary key (competent_department_qualification_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
