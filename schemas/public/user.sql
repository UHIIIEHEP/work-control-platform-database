do $$
  begin

    -- Sequence

    create sequence if not exists public.user_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.user
    (
      user_id integer default nextval('public.user_seq'::regclass) not null
    );

    alter sequence public.user_seq owned by "user".user_id;

    alter table public.user add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.user add column if not exists modified timestamp without time zone default null;
    alter table public.user add column if not exists created_by integer default null;
    alter table public.user add column if not exists modified_by integer default null;
    alter table public.user add column if not exists deleted integer default null;
    alter table public.user add column if not exists organisation_id integer default null;
    alter table public.user add column if not exists firstname character varying;
    alter table public.user alter column firstname set not null;
    alter table public.user add column if not exists lastname character varying;
    alter table public.user alter column lastname set not null;
    alter table public.user add column if not exists patronymic character varying;
    alter table public.user alter column patronymic set not null;
    alter table public.user add column if not exists login character varying;
    alter table public.user alter column login set not null;
    alter table public.user add column if not exists email character varying;
    alter table public.user alter column email set not null;
    alter table public.user add column if not exists p_hash character varying;
    alter table public.user alter column p_hash set not null;
    alter table public.user add column if not exists photo character varying;
    alter table public.user add column if not exists department_id integer;
    alter table public.user add column if not exists qualification_id integer;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'user', 'pk_user')) = false) then

          alter table public.user add
            constraint pk_user primary key (user_id);

        end if;

      end
    $constrain$;

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'user', 'uk_user_name')) = false) then

          alter table public.user add
            constraint uk_user_name unique (email, login);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
