do $$
  begin

    -- Sequence

    create sequence if not exists public.organisation_relation_set_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.organisation_relation_set
    (
      organisation_relation_set_id integer default nextval('public.organisation_relation_set_seq'::regclass) not null
    );

    alter sequence public.organisation_relation_set_seq owned by organisation_relation_set.organisation_relation_set_id;

    alter table public.organisation_relation_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.organisation_relation_set add column if not exists modified timestamp without time zone default null;
    alter table public.organisation_relation_set add column if not exists created_by integer default null;
    alter table public.organisation_relation_set add column if not exists modified_by integer default null;
    alter table public.organisation_relation_set add column if not exists deleted integer default null;
    alter table public.organisation_relation_set add column if not exists relation public.organisation_relation;
    alter table public.organisation_relation_set alter column relation set not null;
    alter table public.organisation_relation_set add column if not exists relation_id integer;
    alter table public.organisation_relation_set alter column relation_id set not null;
    alter table public.organisation_relation_set add column if not exists organisation_id integer;
    alter table public.organisation_relation_set alter column organisation_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'organisation_relation_set', 'pk_organisation_relation_set')) = false) then

          alter table public.organisation_relation_set add
            constraint pk_organisation_relation_set primary key (organisation_relation_set_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
