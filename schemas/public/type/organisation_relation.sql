do $$
  begin

    if ((select * from pkg_db_util.pg_type_exists('public', 'organisation_relation')) = false) then

      create type public.organisation_relation as enum();

    end if;

    alter type public.organisation_relation add value if not exists 'department';
    alter type public.organisation_relation add value if not exists 'qualification';

    comment on type public.organisation_relation
        is 'organisation relation type';

  end
$$;
