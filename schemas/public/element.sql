do $$
  begin

    -- Sequence

    create sequence if not exists public.element_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.element
    (
      element_id integer default nextval('public.element_seq'::regclass) not null
    );

    alter sequence public.element_seq owned by element.element_id;

    alter table public.element add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.element add column if not exists modified timestamp without time zone default null;
    alter table public.element add column if not exists created_by integer default null;
    alter table public.element add column if not exists modified_by integer default null;
    alter table public.element add column if not exists deleted integer default null;
    alter table public.element add column if not exists name character varying;
    alter table public.element alter column name set not null;
    alter table public.element add column if not exists parent_id integer;
    alter table public.element add column if not exists job_id integer;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'element', 'pk_element')) = false) then

          alter table public.element add
            constraint pk_element primary key (element_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
