do $$
  begin

    -- Sequence

    create sequence if not exists public.object_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.object
    (
      object_id integer default nextval('public.object_seq'::regclass) not null
    );

    alter sequence public.object_seq owned by object.object_id;

    alter table public.object add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.object add column if not exists modified timestamp without time zone default null;
    alter table public.object add column if not exists created_by integer default null;
    alter table public.object add column if not exists modified_by integer default null;
    alter table public.object add column if not exists deleted integer default null;
    alter table public.object add column if not exists name character varying;
    alter table public.object alter column name set not null;
    alter table public.object add column if not exists parent_id integer;
    alter table public.object add column if not exists element_id integer;
    alter table public.object add column if not exists organisation_id integer;
    alter table public.object alter column organisation_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'object', 'pk_object')) = false) then

          alter table public.object add
              constraint pk_object primary key (object_id);

        end if;

      end
    $constrain$;

    -- Indexes


    -- Triggers

  end
$$;
