do $$
  begin

    -- Sequence

    create sequence if not exists public.session_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.session
    (
      session_id integer default nextval('public.session_seq'::regclass) not null
    );

    alter sequence public.session_seq owned by session.session_id;

    alter table public.session add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.session add column if not exists modified timestamp without time zone default null;
    alter table public.session add column if not exists created_by integer default null;
    alter table public.session add column if not exists modified_by integer default null;
    alter table public.session add column if not exists deleted integer default null;
    alter table public.session add column if not exists user_id integer;
    alter table public.session alter column user_id set not null;
    alter table public.session add column if not exists address character varying;
    alter table public.session add column if not exists client character varying;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'session', 'pk_session')) = false) then

          alter table public.session add
            constraint pk_session primary key (session_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
