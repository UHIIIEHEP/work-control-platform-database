do $$
  begin

    -- Sequence

    create sequence if not exists public.qualification_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.qualification
    (
      qualification_id integer default nextval('public.qualification_seq'::regclass) not null
    );

    alter sequence public.qualification_seq owned by qualification.qualification_id;

    alter table public.qualification add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.qualification add column if not exists modified timestamp without time zone default null;
    alter table public.qualification add column if not exists created_by integer default null;
    alter table public.qualification add column if not exists modified_by integer default null;
    alter table public.qualification add column if not exists deleted integer default null;
    alter table public.qualification add column if not exists name character varying;
    alter table public.qualification alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'qualification', 'pk_qualification')) = false) then

          alter table public.qualification add
            constraint pk_qualification primary key (qualification_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
