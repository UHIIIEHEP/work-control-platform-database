do $$
  begin

    -- Sequence

    create sequence if not exists public.organisation_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.organisation
    (
      organisation_id integer default nextval('public.organisation_seq'::regclass) not null
    );

    alter sequence public.organisation_seq owned by organisation.organisation_id;

    alter table public.organisation add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.organisation add column if not exists modified timestamp without time zone default null;
    alter table public.organisation add column if not exists created_by integer default null;
    alter table public.organisation add column if not exists modified_by integer default null;
    alter table public.organisation add column if not exists deleted integer default null;
    alter table public.organisation add column if not exists name character varying;
    alter table public.organisation alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'organisation', 'pk_organisation')) = false) then

          alter table public.organisation add
            constraint pk_organisation primary key (organisation_id);

        end if;

      end
    $constrain$;


    -- Indexes

    create index if not exists idx_organisation_id on public.organisation (organisation_id);

    -- Triggers



  end
$$;
