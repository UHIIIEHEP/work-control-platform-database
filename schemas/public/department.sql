do $$
  begin

    -- Sequence

    create sequence if not exists public.department_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.department
    (
      department_id integer default nextval('public.department_seq'::regclass) not null
    );

    alter sequence public.department_seq owned by department.department_id;

    alter table public.department add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.department add column if not exists modified timestamp without time zone default null;
    alter table public.department add column if not exists created_by integer default null;
    alter table public.department add column if not exists modified_by integer default null;
    alter table public.department add column if not exists deleted integer default null;
    alter table public.department add column if not exists name character varying;
    alter table public.department alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'department', 'pk_department')) = false) then

            alter table public.department add
                constraint pk_department primary key (department_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
