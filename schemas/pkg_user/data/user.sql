do $$
  begin

    insert into
      public.user
        (created_by, organisation_id, firstname, lastname, patronymic, login, email, p_hash)
      values

        (-1, 1, 'ИнженерРЗА', 'Ф', 'О', 'rzarza', 'rza@mail.ru', '111111'),
        (-1, 1, 'ОткделКадров', 'Ф', 'О', 'otdotd', 'otd@mail.ru', '111111')

    on conflict do nothing;

  end
$$;