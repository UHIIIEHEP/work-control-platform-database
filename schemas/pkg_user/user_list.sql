-- drop function if exists pkg_user.user_list(integer, integer[], jsonb);

create or replace function pkg_user.user_list (
  p_organisation integer,
  p_user_id integer[] default null,
  p_filter jsonb default null
)

returns table (
  user_id integer,
  organisation_id integer,
  firstname character varying,
  lastname character varying,
  patronymic character varying,
  email character varying,
  department_id integer,
  department_name character varying,
  qualification_id integer,
  qualification_name character varying,
  photo character varying
)

as $$

declare

begin

  return
    query
      select
        u.user_id,
        u.organisation_id,
        u.firstname,
        u.lastname,
        u.patronymic,
        u.email,
        u.department_id,
        d.name,
        u.qualification_id,
        q.name,
        u.photo
      from
        public.user u
        left join public.department d using(department_id)
        left join public.qualification q using(qualification_id)
      where
        u.deleted is null and
        u.organisation_id = p_organisation and

        case
          when
            p_user_id is not null and
            array_length(p_user_id, 1) > 0
              then u.user_id = any(p_user_id)
              else true
          end and

        case
          when
            p_filter -> 'department_id' is not null
            then u.department_id = (p_filter -> 'department_id')::integer
            else true
          end and

        case
          when
            p_filter -> 'qualification_id' is not null
            then u.department_id = (p_filter -> 'qualification_id')::integer
            else true
          end;

end;

$$

language plpgsql;
