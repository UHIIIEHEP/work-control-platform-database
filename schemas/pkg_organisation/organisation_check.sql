create or replace function pkg_organisation.organisation_check (
  p_organisation_id integer
)

returns boolean

as $$

  declare

    v_organisation_id     integer;

  begin

    select
      o.organisation_id
    into
      v_organisation_id
    from
      public.organisation o
    where
      o.organisation_id = p_organisation_id and
      o.deleted is null;

    if v_organisation_id is null then
      raise 'organisation_id_not_found';
    end if;

    return true;

  end;

$$

language plpgsql;