-- drop function if exists pkg_organisation.organisation_list(integer[]);

create or replace function pkg_organisation.organisation_list (
  p_organisation_id integer[] default null
)

returns table (
  organisation_id integer,
  name character varying
)

as $$

declare

begin

  return
    query
      select
        o.organisation_id,
        o.name
      from
        public.organisation o
      where
        deleted is null and
        case
          when
            p_organisation_id is not null and
            array_length(p_organisation_id, 1) > 0
              then o.organisation_id = any(p_organisation_id)
              else true
          end;

end;

$$

language plpgsql;

-- select * from pkg_organisation.organisation_list()
