create or replace function pkg_organisation.organisation_relation_set (
  p_user_id integer,
  p_relation public.organisation_relation,
  p_relation_id integer[],
  p_append boolean default true
)

returns void

as $$

  declare

    v_relation_id       integer;
    v_organisation_id   integer;

  begin

    if p_append is false then
      delete
        from
          public.organisation_relation_set ors
        where
          ors.relation_id = p_relation_id and
          ors.relation = p_relation;
    end if;

    select
      u.organisation_id
    into
      v_organisation_id
    from
      public.user u
    where
      u.user_id = p_user_id and
      u.deleted is null;

    if v_organisation_id is null then
      raise 'organisation_id_not_found';
    end if;

    foreach v_relation_id in array p_relation_id
      loop

        if (
          select
            ors.relation_id
          from
            public.organisation_relation_set ors
          where
            ors.relation = p_relation_id and
            ors.relation_id = v_relation_id
        ) is null then

          insert into public.organisation_relation_set (
            created_by,
            relation,
            relation_id,
            organisation_id
          ) values (
            p_user_id,
            p_relation,
            p_relation_id,
            v_organisation_id
          );

        end if;

      end loop;

  end;
$$

language plpgsql;