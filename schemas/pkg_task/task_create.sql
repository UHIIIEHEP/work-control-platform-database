
create or replace function pkg_task.task_create (
    p_user_id integer,
    p_object_id integer,
    p_title character varying,
    p_description text,
    p_accountable_id integer,
    p_assistants integer[] default null,
    p_application_id integer default null,
    p_work_type public.work_type default null,
    p_date_to timestamp without time zone default null,
    p_date_from timestamp without time zone default null
)

returns integer

as $$

  declare

    v_task_id     integer;

  begin

    insert into public.task (
      created_by,
      title,
      description,
      object_id,
      accountable_id,
      assistants,
      application_id,
      work_type,
      date_to,
      date_from
    ) values (
      p_user_id,
      p_title,
      p_description,
      p_object_id,
      p_accountable_id,
      p_assistants,
      p_application_id,
      p_work_type,
      p_date_to,
      p_date_from
    )
    returning task_id into v_task_id;

    return v_task_id;

  end;

$$

language plpgsql;
